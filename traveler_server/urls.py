from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # AMF Remoting Gateway - interfejs
    (r'^gateway/', 'traveler_server.traveler.amfgateway.gateway'),
    (r'^client/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'bin_flex'}),
    # Example:
    # (r'^traveler_server/', include('traveler_server.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
)
