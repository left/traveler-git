from models import Traveler, City, Route
from django.db.models import Count

import sys, os
sys.path.append(os.getcwd())
import tsolver

def getTravelers():
    """Zwraca liste slownikow wlasnosci wszystkich komiwojazerow"""
    travelers = []
    for traveler in Traveler.objects.all():
        travelers.append( {"id" : traveler.id, "name" : traveler.name, "max_cities" : traveler.max_cities } )
    return travelers

def getTravelerDetails(t_id):
    """Zwraca szczegoly calego komiwojazera - miasta i trasy miedzy nimi"""
    tr = Traveler.objects.get(id = t_id)
    return tr.getDetails()

def addTraveler(t_name, t_max_cities):
    """Dodaje do bazy danych komiwojazera dana liczba miast i nazwa"""
    tr = Traveler(name = t_name, max_cities = int(t_max_cities))
    tr.save()
    return tr.id

def delTraveler(t_id):
    """Usuwa komiwojazera po id"""
    tr = Traveler.objects.get(id = t_id)
    tr.delete()
    
def getCityDetails(c_id):
    """Zwraca szczegoly miasta - wlasnosci i jego trasy"""
    ct = City.objects.get(id = c_id)
    return ct.getDetails()

def addCity(t_id, c_name):
    """Dodaje miasto do bazy danych z okreslionym komiwojazerem i nazwa"""
    tr = Traveler.objects.get(id = t_id)
    count = City.objects.filter(traveler = t_id).aggregate(Count('id'))
    if count['id__count'] >= tr.max_cities:
        return 0;
    return tr.addCity(c_name)

def delCity(c_id):
    """Usuwa miasto z bazdy danych po id"""
    ct = City.objects.get(id = c_id)
    ct.delete()

def addRoute(c1_id, c2_id, length):
    """Dodaje strase miedzy danymi miastami o danej dlugosci """
    if c1_id == c2_id:
        return 0;
    city = City.objects.get(id = c1_id)
    try:
        x = float(length)
        if type(x) is float and x != x: x = int(length)
        return city.addRoute(c2_id, length)
    except ValueError:
        print ValueError
        return -1

def delRoute(r_id):
    """Usuwa trase po id"""
    rt = Route.objects.get(id = r_id)
    rt.delete()
    
def solveTraveler(traveler_id):
    """
    Rozwiazuje problem komiwojazera.
    Przekazuje do modulu w c++ liste wszytskich tras po czym wykonuje algorytm ewolucyjny,
    w razie niepowodzenia lapie wyjatek i wysyla wartosc -1 do gui.  
    """
    tr = Traveler.objects.get(id = traveler_id)
    ts = tsolver.TravelerSolver(tr.max_cities)
    citiesAndRelations = tr.getDetails()['cities']
    for city in citiesAndRelations:
        for route in city['relations']:
#            print "" + str(city['id']) + " " + str(route['city_id']) + " " + str(route['distance'])
            ts.setDistance(city['id'], route['city_id'], route['distance'])
            
    res = []
    try:
        ts.solve(100, 500)
        res.append(ts.getFitness())
        names = _resolveCitiesName(ts.getRoute())
        names.append(names[0])
        res.append(names)
#        print res
    except:
        res.append(-1)
        res.append([-1])
        print "Exception occured: cities not properly defined"
    return res

def _resolveCitiesName(cities_id_list):
    """"Pomocnicza funkcja zaminaiajaca liste id miast na liste solownikow ich wlasnosci"""
    names = [] 
    for city_id in cities_id_list:
        city = City.objects.get(id = city_id)
        names.append( { 'name': city.name, 'id': city_id, } )
    return names
