from pyamf.remoting.gateway.django import DjangoGateway

import traveler_server.traveler.commands

def echo(data):
    return data

services = {
    'traveler.echo' : echo,
    # could include other functions as well
    'traveler.getTravelers' : traveler_server.traveler.commands.getTravelers,
    'traveler.getTravelerDetails' : traveler_server.traveler.commands.getTravelerDetails,
    'traveler.addTraveler' : traveler_server.traveler.commands.addTraveler,
    'traveler.delTraveler' : traveler_server.traveler.commands.delTraveler,
    'traveler.getCityDetails' : traveler_server.traveler.commands.getCityDetails,
    'traveler.addCity' : traveler_server.traveler.commands.addCity,
    'traveler.delCity' : traveler_server.traveler.commands.delCity,
    'traveler.addRoute' : traveler_server.traveler.commands.addRoute,
    'traveler.delRoute' : traveler_server.traveler.commands.delRoute,
    'traveler.solveTraveler' : traveler_server.traveler.commands.solveTraveler,
}

gateway = DjangoGateway(services, expose_request=False, debug=True)
