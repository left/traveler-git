from django.db import models
from django.db.models import Q
from django.db.models import Count

# komiwojazer
class Traveler(models.Model):
    """Model komiwojazera w bazie danych"""
    name = models.CharField(max_length=64)
    max_cities = models.IntegerField()
    
    def __unicode__(self):
        return self.name
    
    def getDetails(self):
        return { 'id' : self.id,
                 'name' : self.name,
                 'max_cities' : self.max_cities,
                 'cities' : self.getCitiesDetails(), }

    def getCities(self):
        cities = City.objects.filter(traveler = self)
        return cities

    #alias for getter - property
    cities = property(getCities)

    def getCitiesDetails(self):
        details = []
        cities = self.getCities()
        for city in cities:
            details.append(city.getDetails())
        return details

    def addCity(self, n):
        city = City(traveler = self, name = n)
        city.save()
        return city.id
    
# miasto przez ktore ma przejechac komiwojazer
class City(models.Model):
    """Model miasta w bazie danych - jest zwiazane z komiwojazerem"""
    traveler = models.ForeignKey(Traveler)
    name = models.CharField(max_length=64)
    
    def __unicode__(self):
        return self.name
    
    def getDetails(self):
        return { 'id' : self.id,
                 'name' : self.name,
                 'relations' : self.getRelatedCities(), }
        
    def getRelatedCities(self):
        routes = Route.objects.filter(Q(city1 = self) | Q(city2 = self))
        cities = []
        for route in routes:
            if route.city1 == self:
                cities.append( {'id' : route.id,
                                'distance' : route.length,
                                'city_id' : route.city2.id,
                                'city_name' : route.city2.name, } )
            else:
                cities.append( {'id' : route.id,
                                'distance' : route.length,
                                'city_id' : route.city1.id,
                                'city_name' : route.city1.name, } )
        return cities
                
    def addRoute(self, city, distance):
        c2 = City.objects.get(id = city)
        if self.traveler != c2.traveler:
            return 0;
        count1 = Route.objects.filter(city1 = self, city2 = c2).aggregate(Count('id'))['id__count']
        count2 = Route.objects.filter(city1 = c2, city2 = self).aggregate(Count('id'))['id__count']
        if count1 > 0 or count2 > 0:
            return 0;
        route = Route(city1 = self, city2 = c2, length = distance)
        route.save()
        return route.id

# droga
class Route(models.Model):
    """Model trasy w bazie danych - trasa jest zwiazana z dwoma miastami"""
    city1 = models.ForeignKey(City, related_name = 'route_to_city1')
    city2 = models.ForeignKey(City, related_name = 'route_to_city2')
    length = models.FloatField()
    
    class Meta:
        unique_together = ("city1", "city2")

    