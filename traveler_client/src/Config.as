// Anna Kopertowska, Przemyslaw Lewandowski - Projekt ZPR
package {
	import mx.messaging.management.Attribute;
	
	/**
	 * Klasa posiadajaca konfiguracje aplikacji, glownie amf.
	 * Singleton.
	 */
	public class Config {
		
		/** Adres do uslug amf. */
		private static const AMF_SERVER_ : String = "http://127.0.0.1:8000/gateway/";
		/** Miejsce odbioru ?. */
		private static const AMF_CLIENT_ : String = "traveler";
		
		/** Instancja singletonu. */
		private static var instance_ : Config = null;
		
		/* 
		   na razie te zmienne sa kopiowane ze staticow,
		   ale mozna by zrobic cos co samo rozpoznaje z
		   jakim server sie polaczyc
		*/
		private var server : String;
		private var client : String;
		
		/** Meotda zwracajaca instancje singletonu. */
		public static function getInstance() : Config {
			if(instance_ == null) {
				instance_ = new Config(arguments.callee);
			}
			return instance_;
		}
		
		/** Wyjatek rzuca c-tor bo singleton. */
		public function Config(caller : Function = null) {
			if(caller != Config.getInstance)
				throw new Error('Singleton!');
			server = AMF_SERVER_;
			client = AMF_CLIENT_;
		}
		
		/** Getter do servera. */
		public function getServer() : String {
			return server;
		}
		
		/** Getter do klienta */
		public function getClient() : String {
			return client;
		}

	}
}