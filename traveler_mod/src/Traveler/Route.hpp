/*
 * Route.hpp
 *
 *  Created on: Dec 26, 2009
 *      Author: Przemyslaw Lewandowski
 */

#ifndef ROUTE_HPP_
#define ROUTE_HPP_

//#include "TravelerSpace.hpp"

#include <vector>
//#include <faif/search/EvolutionaryAlgorithm.hpp>

namespace traveler {

// forward declaration
class TravelerSpace;

/**
 * Klasa reprezentujaca pojedyncza trase komiwojazera.
 */
class Route {
public:
	/** Reprezentacja listy numer kolejnych mias na trasie. */
	typedef std::vector<int> CitiesNumbers;

	/** c-tor - tworzy losowa trase */
	Route(const TravelerSpace& space);
	/** c-tor - ustawia trase. */
	Route(const TravelerSpace& space, const CitiesNumbers& route);
	/** c-tor kopiujacy */
	Route(const Route& r);
	/** O-tor przypisania */
	Route& operator=(const Route& route);

	/** Mutacja trasy. */
	Route& mutate();
	/** Mutacja trasy. */
	void cross(Route& route);
	/** Funkcja dopasowania jako dlugosc trasy. */
	double fitness() const;
	/** Normalizuje ze sprytnej reprezentacji na reprezentacje czytelna. Zwraca trase dla wzoru odniesienia {1, 2, 3, ...}. */
	static CitiesNumbers normalize(const CitiesNumbers& cnumbers);
	/** Getter na liste trasy. */
	CitiesNumbers getRoute() const { return route_; }

private:
	/** Odwolanie do przestrzeni algorytmu. */
	const TravelerSpace& space_;
	/** Kolejne miasta na trasie wzgledem odnisienia: { 1, 2, 3, 4, ... }. */
	CitiesNumbers route_;
	/** Znacznik czy wartosc f-cji dopasowania jest aktualna. */
	mutable bool fitValueValid_;
	/** Wartosc f-cji dopasowania. */
	mutable double fitValue_;
};

} //namespace traveler

#endif /* ROUTE_HPP_ */
