/*
 * Traveler_py.hpp
 *
 *  Created on: Jan 6, 2010
 *      Author: Przemyslaw Lewandowski
 */

#ifndef TRAVELER_PY_HPP_
#define TRAVELER_PY_HPP_

#include "Traveler.hpp"
#include "Route.hpp"
#include "TravelerSpace.hpp"

#include <boost/python.hpp>

namespace traveler {

/**
 * Fasada tworzaca spojny interfejs dla problemu komiwojazera, a takze
 * adapter dopasowujacy interfejs do modulu w Python'ie.
 */
class Traveler_py {
public:
	/**
	 * c-tor
	 *
	 * @param citiesNumber Liczba miast przekazana skladowej Traveler.
	 */
	Traveler_py(int citiesNumber);
	/** Ustawia odleglosc miedzy miastami. */
	void setDistance(int id1, int id2, Traveler::Distance value);
	/** Rozwiazuje problem komiwojazera. */
	void solve(int stepsNumber, int populationSize);
	/** Zwraca najlepsza trase uzywajaz id z bazy. */
	boost::python::list getRoute() const;
	/** Zwraca najkrotsza droge. */
	TravelerSpace::Fitness getFitness() const;

private:
	/** Obiekt reprezentujacy problem komiwojazera. */
	Traveler traveler;
	/** Najlepsza znaleziona trasa - zapisana juz w postaci id z bazy danych. */
	Route::CitiesNumbers bestRoute;
	/** Dlugosc najkrotszej znalezionej trasy.*/
	TravelerSpace::Fitness bestFitness;
};

} // namespace

#endif /* TRAVELER_PY_HPP_ */
