/*
 * Route.cpp
 *
 *  Created on: Dec 26, 2009
 *      Author: Przemyslaw Lewandowski
 */

#include "Route.hpp"
#include "Traveler.hpp"
#include "TravelerSpace.hpp"

#include <algorithm>
#include <faif/utils/Random.h>

namespace traveler {

// fills route_ with random cities numbers
Route::Route(const TravelerSpace& space) : space_(space), fitValueValid_(false), fitValue_(0.0) {
	int n = space_.getTraveler().getCitiesCount();
	for(int i = 0; i < n; ++i) {
		faif::RandomInt gen(0, n-i-1);
		route_.push_back(gen());
	}
}

Route::Route(const TravelerSpace& space, const CitiesNumbers& route) : space_(space), route_(route),
	fitValueValid_(false), fitValue_(0.0) { /* empty */ }

Route::Route(const Route& r) : space_(r.space_), route_(r.route_),
	fitValueValid_(r.fitValueValid_), fitValue_(r.fitValue_) { /* empty */}

Route& Route::operator=(const Route& route) {
	fitValueValid_ = route.fitValueValid_;
	fitValue_ = route.fitValue_;
	route_ = route.route_;
	// space_ = route.space_; // space jest zawsze ten sam, a po za tym to refencja sobie jest
	return *this;
}

Route::CitiesNumbers Route::normalize(const CitiesNumbers& cnumbers) {
	int size = cnumbers.size();
	std::vector<bool> removed(size);
	CitiesNumbers ret;

	for(CitiesNumbers::const_iterator it = cnumbers.begin(); it != cnumbers.end(); ++it) {
		int counter = 0;
		for(int i = 0; i <= *it; ) {
			if(!removed[counter])
				++i;
			++counter;
		}
		removed[counter-1] = true;
		ret.push_back(counter-1);
	}
	return ret;
}

double Route::fitness() const {
	if(!fitValueValid_) {
		CitiesNumbers normalized = normalize(route_);
		const Traveler& tr = space_.getTraveler();
		double sum = tr.getDistance(normalized.back(), normalized.front());
		for(CitiesNumbers::const_iterator it = normalized.begin(), backIt(it++);
				it != normalized.end(); ++it, ++backIt) {
			sum += tr.getDistance(*it,  *backIt);
		}
		fitValue_ = -sum; // ea maxymalizuje f-cje dopasowania dlatego zwraca jest wartosc przeciwna
		fitValueValid_ = true;
	}
    return fitValue_;
}

void Route::cross(Route& route) {
	int n = space_.getTraveler().getCitiesCount();
	faif::RandomInt gen(0, n-1);
	CitiesNumbers::iterator middle = route.route_.begin();
	std::advance(middle, gen());
	std::swap_ranges(route.route_.begin(), middle, route_.begin());
	fitValueValid_ = route.fitValueValid_ = false;
}

Route& Route::mutate() {
	int n = space_.getTraveler().getCitiesCount();
	faif::RandomInt genNumber(0, n-1);
	int i = genNumber();
	faif::RandomInt gen(0, n-i-1);
	route_.at(i) = gen();
	fitValueValid_ = false;
	return *this;
}

} // namespace traveler
