/*
 * TravelerStopCondition.hpp
 *
 *  Created on: Dec 27, 2009
 *      Author: Anna Kopertowska
 */

#ifndef TRAVELERSTOPCONDITION_HPP_
#define TRAVELERSTOPCONDITION_HPP_

#include <faif/utils/actobj/Command.h>

namespace traveler {

/**
 * Klasa implementujaca warunek stopu dla algorytmu ewolucyjnego.
 */
class TravelerStopCondition {
public:
	/** c-tor */
	TravelerStopCondition(int numberSteps);

	/** Element interfejsu potrzebny do algorytmu ewolucyjnego. */
	template<typename Population> void update(const Population& ) {
		++step_;
	}

	/** Element interfejsu potrzebny do algorytmu ewolucyjnego. */
	bool isFinished() const;

private:
	/** Aktualny krok algorytmu. */
	int step_;
	/** Liczba krokow jaka ma sie odbyc. */
	int numSteps_;
};

}

#endif /* TRAVELERSTOPCONDITION_HPP_ */
