/*
 * Traveler.cpp
 *
 *  Created on: Dec 27, 2009
 *      Author: Anna Kopertowska
 */

#include "Traveler.hpp"

#include "TravelerSpace.hpp"
#include "TravelerStopCondition.hpp"

#include <algorithm>
#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <faif/search/EvolutionaryAlgorithm.hpp>

namespace traveler {

Traveler::Traveler(int numCities) : distances_(numCities), citiesCount_(numCities), cityCounter_(0) {
	int i=0;
	for(DistanceArray::iterator it = distances_.begin(); it != distances_.end(); ++it, ++i)
		it->resize(i);
}

Traveler::Distance Traveler::getDistance(const int& num1, const int& num2) const {
	if(num1 > num2)
		return distances_.at(num1).at(num2);
	return distances_.at(num2).at(num1);
}

Traveler::Distance Traveler::getDistanceById(const int& id1, const int& id2) const {
	return getDistance(mapIdToNumber.at(id1), mapIdToNumber.at(id2));
}

void Traveler::setDistance(int id1, int id2, Distance value) {
	if(mapIdToNumber.find(id1) == mapIdToNumber.end()) {
		int tmp = getNextCityNumber();
		mapIdToNumber[id1] = tmp;
		mapNumberToId[tmp] = id1;
	}
	if(mapIdToNumber.find(id2) == mapIdToNumber.end()) {
		int tmp = getNextCityNumber();
		mapIdToNumber[id2] = tmp;
		mapNumberToId[tmp] = id2;
	}
	id1 = mapIdToNumber[id1];
	id2 = mapIdToNumber[id2];

	if(id1 > id2) {
		distances_.at(id1).at(id2) = value;
		return;
	}
	distances_.at(id2).at(id1) = value;
}

Route::CitiesNumbers Traveler::resolveRoute(const Route::CitiesNumbers& route) const {
	Route::CitiesNumbers result;
//	std::transform(route.begin(), route.end(), result.begin(), boost::bind(&Traveler::getIdByNumber, boost::ref(*this), _1));
	for(Route::CitiesNumbers::const_iterator it = route.begin(); it != route.end(); ++it)
		result.push_back(getIdByNumber(*it));
	return result;
}

void Traveler::solve(int stepsNumber, int populationSize) {

	if(cityCounter_ != citiesCount_)
		throw CitiesNotProperlyDefined();

	TravelerSpace space(*this);
	TravelerSpace::Population population;

	for(int i = 0; i < populationSize; ++i )
		population.push_back( Route(space) );

	faif::search::EvolutionaryAlgorithm<TravelerSpace,
										faif::search::MutationCustom,
										faif::search::SelectionTournament,
										TravelerStopCondition,
										faif::search::CrossoverCustom > ealg;

	TravelerStopCondition stop(stepsNumber);
	bestRoute = PRoute( new Route(ealg.solve(population, stop)) );
}

int Traveler::getNextCityNumber() {
	return cityCounter_++;
}

}
