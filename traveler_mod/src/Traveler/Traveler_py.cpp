/*
 * Traveler_py.cpp
 *
 *  Created on: Jan 6, 2010
 *      Author: Przemyslaw Lewandowski
 */

#include "Traveler_py.hpp"

namespace traveler {

Traveler_py::Traveler_py(int citiesNumber) : traveler(citiesNumber) {
	// empty
}

void Traveler_py::setDistance(int id1, int id2, Traveler::Distance value) {
	traveler.setDistance(id1,id2,value);
}

void Traveler_py::solve(int stepsNumber, int populationSize) {
	traveler.solve(stepsNumber, populationSize);
}

boost::python::list Traveler_py::getRoute() const {
	boost::python::list out;
	Route::CitiesNumbers cities = traveler.resolveRoute( Route::normalize( traveler.getBestRoute()->getRoute() ) );
	for(Route::CitiesNumbers::const_iterator it = cities.begin(); it != cities.end(); ++it) {
		out.append(*it);
	}
	return out;
}

TravelerSpace::Fitness Traveler_py::getFitness() const {
	return -traveler.getBestRoute()->fitness();
}

} // namespace
