/*
 * Traveler.hpp
 *
 *  Created on: Dec 27, 2009
 *      Author: Anna Kopertowska
 */

#ifndef TRAVELER_HPP_
#define TRAVELER_HPP_

#include "Route.hpp"

#include <vector>
#include <exception>
#include <boost/unordered_map.hpp>
#include <boost/shared_ptr.hpp>

namespace traveler {

/**
 * Pomocnicza klasa definiujaca wyjatek rzucany na rzadanie
 * rozwiazania problemu komiwojazera przed poprawnym zdefiniowaniem miast.
 */
class CitiesNotProperlyDefined : public std::exception {};

/**
 * Klasa reprezentuje problem komiwojazera.
 * Przechowuje miasta i odlegosci miedzy nimi.
 */
class Traveler {
public:
//	typedef long CityId;
	/** Pomocniczy typ okreslajacy odleglosc miedzy miastami. */
	typedef double Distance;
	/** Reprezentacja odleglosci miedzy miastami - w zalozeniu trojkatna macierz. */
	typedef std::vector< std::vector<Distance> > DistanceArray;
	/** Wskanik na Route, przydatny do zapamietania najlepszej wyliczonej trasy. */
	typedef boost::shared_ptr<Route> PRoute;

	/** c-tor */
	Traveler(int numCities);
	/**	d-tor */
	~Traveler() {}

	/** Zwraca odleglosc miedzy miastami po numerach miast. */
	Distance getDistance(const int& num1, const int& num2) const;
	/** Zwraca odleglosc miedzy miastami po id miast z bazy. */
	Distance getDistanceById(const int& id1, const int& id2) const;
	/** Ustawia odleglosc miedzy miastami. */
	void setDistance(int id1, int id2, Distance value);
	/** Zwraca id przez dany numer w reprezentacji. */
	int getIdByNumber(const int& number) const { return mapNumberToId.at(number); };
	/** Zwraca numer w reprezentacji przez dane id . */
	int getNumberById(const int& number) const { return mapIdToNumber.at(number); };
	/** Konweruje trase ze wzorca na trase z bazy danych. */
	Route::CitiesNumbers resolveRoute(const Route::CitiesNumbers& route) const;
	/** Rozwiazuje problem komiwojazera. */
	void solve(int stepsNumber, int populationSize);
	/** Zwraca najlepsza znaleiozna trase. */
	PRoute getBestRoute() const { return bestRoute; }
	/** Getter liczby miast. */
	int getCitiesCount() const {
		return citiesCount_;
	}

private:
	/** Zwraca obecna wartosc cityCouter i inkrementuje ja. */
	int getNextCityNumber();

	/** Typ hash mapy dla tlumaczenia z id miasta w bazie danych na znormalizowane. */
	typedef boost::unordered_map<int, int> NormalizeMap;

	/** Maciez odleglosci miedzy miastami. */
	DistanceArray distances_;
	/** Liczba miast */
	int citiesCount_;
	/** Licznik ktory nadaje kolejne numery miastom podawanych w setDistance(). */
	int cityCounter_;
	/** Mapa tlumaczaca z id na numer w reprezentacji. */
	NormalizeMap mapIdToNumber;
	/** Mapa tlumaczaca z numeru w reprezentacji na id. */
	NormalizeMap mapNumberToId;

	/** Najlepsza znaleziona trasa. */
	PRoute bestRoute;
};

} // namespace traveler

#endif /* TRAVELER_HPP_ */
