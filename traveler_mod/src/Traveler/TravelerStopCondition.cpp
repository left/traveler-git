/*
 * TravelerStopCondition.cpp
 *
 *  Created on: Dec 27, 2009
 *      Author: Anna Kopertowska
 */

#include "TravelerStopCondition.hpp"

namespace traveler {

TravelerStopCondition::TravelerStopCondition(int numberSteps) : step_(0), numSteps_(numberSteps) { /* empty */ }

bool TravelerStopCondition::isFinished() const {
	return step_ >= numSteps_;
}

}
