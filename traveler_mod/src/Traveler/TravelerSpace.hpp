/*
 * TravelerSpace.hpp
 *
 *  Created on: Dec 26, 2009
 *      Author: Przemyslaw Lewandowski
 */

#ifndef TRAVELERSPACE_HPP_
#define TRAVELERSPACE_HPP_

#include "Traveler.hpp"
#include "Route.hpp"

#include <faif/search/EvolutionaryAlgorithm.hpp>

namespace traveler {

/**
 * Klasa opisujaca przestrzen problemu komiwojazera dla algorytmu ewolucyjnego z biblioteki FAIF.
 */
class TravelerSpace : public faif::search::EvolutionaryAlgorithmSpace<Route> {
public:
	/** c-tor */
	TravelerSpace(const Traveler& traveler) : traveler_(traveler) {}

	/** Metoda wolana przez algorytm ewolucyjny. */
	static Individual& mutation(Individual& ind) {
		// mutation
		return ind.mutate();
	}
	/** Metoda wolana przez algorytm ewolucyjny. */
	static void cross(Individual& ind1, Individual& ind2) {
		//crossover
		ind1.cross(ind2);
	}
	/** Metoda wolana przez algorytm ewolucyjny. */
	static Fitness fitness(const Individual& ind) {
		// fitness
		return ind.fitness();
	}
	/** Metoda zwraca rozmiar grupy dla selekcji turniejowej. */
	static int getTournamentSize() { return 3; }
	/** Getter dla obiektu reprezentujacego komiwojazera. */
	const Traveler& getTraveler() const {
		return traveler_;
	}

private:
	/** Problem komiwojazera dla tej przestrzeni. */
	const Traveler& traveler_;
};

} // namespace traveler

#endif /* TRAVELERSPACE_HPP_ */
