/*
 * Solver_py.cpp
 *
 *  Created on: Jan 6, 2010
 *      Author: Przemyslaw Lewandowski
 */

#include "Traveler/Traveler_py.hpp"
#include "Traveler/Traveler.hpp"

#include <boost/python.hpp>

using namespace boost::python;
//using namespace traveler;

void exceptionTranslator(const traveler::CitiesNotProperlyDefined& ) {
    PyErr_SetString(PyExc_UserWarning, "CitiesNotProperlyDefined");
}


BOOST_PYTHON_MODULE( tsolver ) {

    boost::python::class_<traveler::Traveler_py>("TravelerSolver",
		init<int>())
        .def( "setDistance", &traveler::Traveler_py::setDistance )
        .def( "solve", &traveler::Traveler_py::solve )
		.def( "getRoute", &traveler::Traveler_py::getRoute )
        .def( "getFitness", &traveler::Traveler_py::getFitness )
        ;

    register_exception_translator<traveler::CitiesNotProperlyDefined>(exceptionTranslator);
}
