/*
 * RouteTest.cpp
 *
 *  Created on: Jan 6, 2010
 *      Author: Przemyslaw Lewandowski
 */
#define BOOST_TEST_DYN_LINK
//#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

#include "../src/Traveler/Traveler.hpp"
#include "../src/Traveler/TravelerSpace.hpp"
#include "../src/Traveler/Route.hpp"

using boost::unit_test::test_suite;
using namespace boost::unit_test;
using namespace std;

using namespace traveler;

BOOST_AUTO_TEST_SUITE( RouteTest )

/*
 * Testuje metodes statyczna normalizujaca sprytny zapis na zapis wg wzorca: {1, 2, 3, 4, ...}
 */
BOOST_AUTO_TEST_CASE( NormalizeTest ) {
	Traveler t(2);
	TravelerSpace s(t);

	int tabCities[] = {1, 3, 2, 0, 0};
	int tabResult[] = {1, 4, 3, 0, 2};

	int tabCitiesLen = sizeof(tabCities) / sizeof(tabCities[0]);

	Route::CitiesNumbers cities(tabCities, tabCities + tabCitiesLen);
	Route::CitiesNumbers normalized(tabResult, tabResult + tabCitiesLen);
	Route r(s, cities);
	Route::CitiesNumbers result = r.normalize(cities);

	for(int i = 0; i < tabCitiesLen; ++i) {
		BOOST_CHECK_EQUAL(result[i], normalized[i]);
//		std::cout << result[i] << " " << normalized[i] << std::endl;
	}
}

/*
 * Testuje funkcje dopasowania dla trasy.
 */
BOOST_AUTO_TEST_CASE( FitnessTest ) {
	Traveler t(3);

	t.setDistance(0, 1, 1.0);
	t.setDistance(1, 2, 2.0);
	t.setDistance(2, 0, 3.0);

	TravelerSpace space(t);
	int tabCities[] = {1, 1, 0};
	int tabCitiesLen = sizeof(tabCities) / sizeof(tabCities[0]);
	Route::CitiesNumbers cities(tabCities, tabCities + tabCitiesLen);
	Route r(space, cities);

	BOOST_CHECK_EQUAL(r.fitness(), -6);
}

BOOST_AUTO_TEST_SUITE_END()
