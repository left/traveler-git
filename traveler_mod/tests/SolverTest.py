# Przemyslaw Lewandowski

#prosty test modulu w pythonie

import sys, os
sys.path.append(os.getcwd() + "/../../")
sys.path.append(os.getcwd())
import tsolver

solver = tsolver.TravelerSolver(3)

solver.setDistance(0,1,1)
solver.setDistance(0,2,2)
solver.setDistance(1,2,3)

solver.solve(3, 3)
if solver.getFitness() == 6:
    print "OK"
else:
    print "test failed"
