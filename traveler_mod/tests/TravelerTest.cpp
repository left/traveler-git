/*
 * TravelerTest.cpp
 *
 *  Created on: Jan 2, 2010
 *      Author: Anna Kopertowska
 */
#define BOOST_TEST_DYN_LINK
// main
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

#include "../src/Traveler/Traveler.hpp"
#include "../src/Traveler/TravelerSpace.hpp"
#include "../src/Traveler/Route.hpp"

using boost::unit_test::test_suite;
using namespace boost::unit_test;
using namespace std;

using namespace traveler;

BOOST_AUTO_TEST_SUITE( TravelerTest )

/*
 * Testuje mapowanie miast z id na numer kolejny ze wzorca: {1, 2, 3, ...}
 * oraz mapowanie odwrotne.
 */
BOOST_AUTO_TEST_CASE( MapingTest ) {

	int idsTab[] = {4, 7, 2, 12, 10, 45};
	vector<int> numbers;
	int tabLen = ( sizeof(idsTab) / sizeof(idsTab[0]) );

	Traveler t(tabLen);

	for(int j = 0, i = 1; i < tabLen; ++j, ++i) {
		t.setDistance(idsTab[j], idsTab[i], i);
		numbers.push_back(t.getNumberById(idsTab[j]));
	}
	numbers.push_back(t.getNumberById(idsTab[tabLen-1]));

	for(int j = 0, i = 1; i < tabLen; ++j, ++i) {
		BOOST_CHECK_EQUAL( t.getDistance(numbers[i], numbers[j]), i );
	}
}

/*
 * Testuje prostu uklad 3 miast - jeden cykl.
 */
BOOST_AUTO_TEST_CASE( SimpleTestForEAOperators ) {

	Traveler t(3);

	t.setDistance(0, 1, 1.0);
	t.setDistance(1, 2, 2.0);
	t.setDistance(2, 0, 3.0);

	TravelerSpace space(t);

	TravelerSpace::Population population;
	for(int i = 0; i < t.getCitiesCount(); ++i )
		population.push_back( Route(space) );

	faif::search::EvolutionaryAlgorithm<TravelerSpace,
										faif::search::MutationCustom,
										faif::search::SelectionRanking,
										faif::search::StopAfterNSteps<30>,
										faif::search::CrossoverCustom > ealg;

	Route& best = ealg.solve( population);
//	Route best(space);
//	std::cout << best.getRoute().size() << " " << best.getRoute()[best.getRoute().size() - 1] << std::endl;
	BOOST_CHECK_EQUAL( -6.0, best.fitness() );
}

/*
 * Testuje uklad 4 miast - jest wiele drog, najkrotsza ma dlugosc 9.
 */
BOOST_AUTO_TEST_CASE( SolveTestForEAOperators ) {
	Traveler t(4);

	t.setDistance(0, 1, 5.0);
	t.setDistance(0, 2, 4.0);
	t.setDistance(0, 3, 1.0);
	t.setDistance(1, 2, 1.0);
	t.setDistance(1, 3, 3.0);
	t.setDistance(2, 3, 3.0);

	TravelerSpace space(t);

	TravelerSpace::Population population;
	for(int i = 0; i < t.getCitiesCount(); ++i )
		population.push_back( Route(space) );

	faif::search::EvolutionaryAlgorithm<TravelerSpace,
										faif::search::MutationCustom,
										faif::search::SelectionRanking,
										faif::search::StopAfterNSteps<30>,
										faif::search::CrossoverCustom > ealg;

	Route& best = ealg.solve( population);
//	Route best(space);
//	std::cout << best.getRoute().size() << " " << best.getRoute()[best.getRoute().size() - 1] << std::endl;
	BOOST_CHECK_EQUAL( -9.0, best.fitness() );
}

BOOST_AUTO_TEST_SUITE_END()
